locals {
  #Simnple example of using a single YAML file to store a map
  transits = yamldecode(file("transits.yml"))

  #More advanced example of automatically combining mulitple files based on a filename pattern:
  spokes = yamldecode(join("\n", [
    for fn in fileset(path.module, "spokes*.yml") : file(fn)
  ]))

  #Create reverse map of regions (key) and their transit (value). This will only work with a single transit in each region, as items need to be unique.
  transit_region_lookup_map = { for k, v in local.transits : v.region => k }
}
