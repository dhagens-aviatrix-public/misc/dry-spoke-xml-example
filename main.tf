#Aviatrix transit
module "transits" {
  source  = "terraform-aviatrix-modules/mc-transit/aviatrix"
  version = "2.1.4"

  for_each = local.transits

  cloud           = each.value.cloud
  cidr            = each.value.cidr
  region          = each.value.region
  account         = each.value.account
  local_as_number = each.value.asn
  ha_gw           = try(each.value.ha_gw, null) #Example of how to deal with optional parameters in the YAML map
}


module "spokes" {
  source  = "terraform-aviatrix-modules/mc-spoke/aviatrix"
  version = "1.2.3"

  for_each = local.spokes

  cloud   = each.value.cloud
  name    = each.key
  cidr    = each.value.cidr
  region  = each.value.region
  account = each.value.account
  ha_gw   = try(each.value.ha_gw, true) #Contrary to the mc-transit module, mc-spoke module does not accept null, as the input variable does not yet have "nullable = false" as property.

  #Find transit name, based on spoke region. This assumes there is only one transit in each region.
  transit_gw = module.transits[lookup(local.transit_region_lookup_map, each.value.region)].transit_gateway.gw_name
}
